//
//  TransactionErr.swift
//  YZOandaAPI
//
//  Created by Bonifatio Hartono on Y17M04D21.
//  Copyright © 2017 Bonifatio Hartono. All rights reserved.
//

import Foundation

public enum TransactionErr : Error {

	/// Thrown during serialization to NSDictionary.. possibly returned Array instead
	case UnexpectedDataType(data: Data)

	/// thrown during checking for Transaction Type.
	case TypeNotFound(dict: NSDictionary)

	/// thrown during mapping the type string to enum and can't find match
	case TypeNotRecognized(type: String)

	/// thrown during checking for Reason
	case ReasonNotFound(dict: NSDictionary)

	/// thrown during mapping the reason string to enum can can't find the match
	case ReasonNotRecognized(reason: String)
    
    /// thrown when unable to find the "id" key
    case IDNotFound(dict: NSDictionary)
    
    /// thrown when unable to find the "tradeID" key
    case TradeIDNotFound(dict: NSDictionary)
    
    /// thrown when unable to find the "price" key
    case PriceNotFound(dict: NSDictionary)
    
    /// thrown when unable to find the "orderID" key
    case OrderIDNotFound(dict: NSDictionary)

	/// thrown when unable to find the "instrument" key
	case InstrumnetNotFound(dict: NSDictionary)
    
    /// thrown when unable to find the "units" key
    case UnitsNotFound(dict: NSDictionary)
    
    /// thrown when unable to find the "tradesClosed" key
    case TradesClosedNotFound(dict: NSDictionary)
    
    /// thrown when unable to find the "realizedPL" key
    case RealizedPLNotFound(dict: NSDictionary)
    
    /// thrown when unable to find the "financing" key
    case FinancingNotFound(dict: NSDictionary)
    
}
