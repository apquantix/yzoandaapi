//
//  URLComponents+YZOandaAPI.swift
//  YZOandaAPI
//
//  Created by Bonifatio Hartono on Y17M04D27.
//  Copyright © 2017 Bonifatio Hartono. All rights reserved.
//

import Foundation

public extension URLComponents {


	init(scheme: YZOandaAPIeScheme) {
		self.init()
		self.scheme = scheme.rawValue
	}


	init(scheme: YZOandaAPIeScheme, host: String){
		self.init(scheme: scheme)
		self.host = host
	}


	init(scheme: YZOandaAPIeScheme, host: String, path: [String]){
		self.init(scheme: scheme, host: host)
		_ = path.map { self.path.append("/" + $0) }
	}


	init(scheme: YZOandaAPIeScheme, host: String, version: String){
		self.init(scheme: scheme, host: host)
		self.path.append(version)
	}

	init(machineName: YZOandaAPIeMachine){
		self.init(scheme: YZOandaAPIeScheme.HTTPS,
		          host: machineName.rawValue + YZOandaAPIeDomain.Oanda.rawValue,
		          version: YZOandaAPIeVersion.v3.rawValue)
	}


	init(machineName: YZOandaAPIeMachine, v3Path: [String]){
		self.init(machineName: machineName)
		_ = v3Path.map { self.path.append("/" + $0) }

	}

	init(machineName: YZOandaAPIeMachine, queryItems: [URLQueryItem], v3Path:[String]) {
		self.init(machineName: machineName, v3Path: v3Path)
		self.queryItems = queryItems
	}


	init(machineName: YZOandaAPIeMachine, accountID: String){
		self.init(machineName: machineName, v3Path: ["accounts"])
		self.path.append("/" + accountID)

	}


	init(machineName: YZOandaAPIeMachine, accountID: String, accountPath: YZOandaAPIeAccount){
		self.init(machineName: machineName, accountID: accountID)
		self.path.append(accountPath.rawValue)
	}

	init(machineName: YZOandaAPIeMachine, accountID: String, accountPath: YZOandaAPIeAccount, queryItems: [URLQueryItem]) {
		self.init(machineName: machineName, accountID: accountID, accountPath: accountPath)
		self.queryItems = queryItems
	}
	init(machineName: YZOandaAPIeMachine, queryItems: [URLQueryItem]) {
		self.init(machineName: machineName)
		self.queryItems = queryItems
	}

}
