//
//  YZOandaAPI.swift
//  YZOandaAPI
//
//  Created by Bonifatio Hartono on Y17M03D21.
//  Copyright © 2017 Bonifatio Hartono. All rights reserved.
//

import Foundation
import YZExtensions

/// TODO: Switch accountID usage to userSelectedAccount
public final class YZOandaAPI {

	public static var authenticated : YZOandaAPI?

	private var baseURL = "https://api-fxtrade.oanda.com"
    private var baseStreamURL = "https://stream-fxtrade.oanda.com"
	private var baseVersion = "v3"
	private var userToken : String
	public var userSelectedAccount : String?


	/* Constructor */
	public init(withToken: String) {
		self.userToken = withToken
		YZOandaAPI.authenticated = self
	}


	/// FIXME: How can we surface the error to caller.
	/// Sending request to server and calling responseCall with dictionary response.
	///	if an array returned by the request, this function will call the callback with nil value.
	/// - Parameters:
	///   - inRequest: URLRequest to be perform with
	///   - responseCall: callback for dictionary response.
	private func letsSend(inRequest: URLRequest, responseCall: @escaping(NSDictionary?) -> () ) {

		inRequest.letsSendJSONRequest { (inDictionary, inArray, inError) in
			if let anError = inError {
				print(anError)
				responseCall(nil)
				return
			}
			if inArray != nil {
				print("Expecting Dictionary")
				responseCall(nil)
				return
			}
			guard let aDictionary = inDictionary else {
				print("Expecting Dictionary but found nil")
				responseCall(nil)
				return
			}
			responseCall(aDictionary)
		}
	}

	/* ACCOUNT REQUEST (does not use query parameter) */
	public func letsRetrieveAccounts(response: @escaping(NSDictionary?) -> () ) {

		let anURLComp = URLComponents(machineName: .fxTradeMachineName, v3Path: ["accounts"] )
		let aRequest = NSMutableURLRequest(withBearer: self.userToken, url: anURLComp.url)
		self.letsSend(inRequest: aRequest as URLRequest, responseCall: response)
	}

	/* SUMMARY REQUEST (does not use query parameter) */
	public func letsRetrieveAccountSummary(accountID: String, response: @escaping(NSDictionary?) -> ()) {

		let anURLComp = URLComponents(machineName: .fxTradeMachineName, accountID: accountID, accountPath: .summary )
		let aRequest = NSMutableURLRequest(withBearer: self.userToken, url: anURLComp.url)
		self.letsSend(inRequest: aRequest as URLRequest, responseCall: response)
	}

	/* ACCOUNT DETAIL REQUEST (does not use query parameter) */
	public func letsRetrieveAccountDetail(accountID: String, response: @escaping(NSDictionary?) -> ()) {

		let anURLComp = URLComponents(machineName: .fxTradeMachineName, accountID: accountID )
		let aRequest = NSMutableURLRequest(withBearer: self.userToken, url: anURLComp.url)
		self.letsSend(inRequest: aRequest as URLRequest, responseCall: response)
	}

	/* PRICING (uses query parameter) */
	public func letsRetrievePrice(accountID: String, instruments: String, response: @escaping (NSDictionary?) -> () ){

		let queryItems = [URLQueryItem(name: "instruments", value: instruments)]
		let anURLComp = URLComponents(machineName: .fxTradeMachineName, accountID: accountID, accountPath: .pricing, queryItems: queryItems)
		let aRequest = NSMutableURLRequest(withBearer: self.userToken, url: anURLComp.url)
		self.letsSend(inRequest: aRequest as URLRequest, responseCall: response)
	}

	public func letsPostOrder(inData: Data){
		if let anAccount = self.userSelectedAccount {
			let urlComp = URLComponents(machineName: .fxTradeMachineName, accountID: anAccount, accountPath: .orders)
			let aRequest = NSMutableURLRequest(withBearer: self.userToken, url: urlComp.url)
			aRequest.httpBody = inData
			aRequest.httpMethod = "POST"
			aRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")

			(aRequest as URLRequest).letsSendRequest { (inData, inError) in
				if let err = inError {
					print(err)
				}
				if let aData = inData {
					print(String(data: aData, encoding: .utf8) ?? "Can't get Data")
				}

			}
		}

	}
    
    /* BASE STREAM URL CONSTRUCTOR, USES QUERY PARAMETER */
    private func letsConstructBaseStreamRequest(with: [URLQueryItem]) -> NSMutableURLRequest{

		let anURLComp = URLComponents(machineName: .fxTradeStreamMachineName, queryItems: with)
		let aRequest = NSMutableURLRequest(withBearer: self.userToken, url: anURLComp.url)
        return aRequest
    }

    /* PRICE STREAM */
	var priceDataTask : URLSessionDataTask? = nil
    public func letsRetrievePriceStream(accountID: String, instruments: String, delegate: URLSessionDelegate ){

		/// Prevent duplicate stream
		self.priceDataTask?.cancel()

		let queryItems = [URLQueryItem(name: "instruments", value: instruments)]
		let anURLComp = URLComponents(machineName: .fxTradeStreamMachineName, accountID: accountID, accountPath: .pricing, queryItems: queryItems)
		let aRequest = NSMutableURLRequest(withBearer: self.userToken, url: anURLComp.url)
        aRequest.url?.appendPathComponent("stream")
        
        // Create NSURLSession to manage the connection to the endpoint
        let aSession: URLSession = URLSession(configuration: URLSessionConfiguration.default, delegate: delegate, delegateQueue: OperationQueue.main)
        
        // Create data task with request that we constructed
        self.priceDataTask = aSession.dataTask(with: aRequest as URLRequest)
        
        // actually perform the connection to the endpoint
        self.priceDataTask?.resume()

    }

	/* Transaction Activity Stream */
	var activityDataTask: URLSessionDataTask? = nil
	public func letsRetrieveTransactionStream(accountID: String, delegate: URLSessionDelegate){

		/// Prevent duplicate stream
		self.activityDataTask?.cancel()

		let anURLComp = URLComponents(machineName: .fxTradeStreamMachineName, accountID: accountID, accountPath: .transactions)
		let aRequest = NSMutableURLRequest(withBearer: self.userToken, url: anURLComp.url)
		aRequest.url?.appendPathComponent("stream")

		// Create NSURLSession to manage the connection to the endpoint
		let aSession: URLSession = URLSession(configuration: URLSessionConfiguration.default, delegate: delegate, delegateQueue: OperationQueue.main)

		// Create data task with request that we constructed
		self.activityDataTask = aSession.dataTask(with: aRequest as URLRequest)

		// actually perform the connection to the endpoint
		self.activityDataTask?.resume()
	}

}
