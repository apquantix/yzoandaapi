//
//  YZOandaAPIModel.swift
//  YZOandaAPI
//
//  Created by Bonifatio Hartono on Y17M04D21.
//  Copyright © 2017 Bonifatio Hartono. All rights reserved.
//

import Foundation

public class YZOandaAPIModel : NSObject {
	public let data: Data
	public var dict: NSDictionary

	public init(data: Data) throws {
		self.data = data
		do {
			guard let aDict = try JSONSerialization.jsonObject(with: data, options: [.mutableContainers, .mutableLeaves]) as? NSDictionary else {
				throw TransactionErr.UnexpectedDataType(data: data)
			}
			self.dict = aDict

		}
		catch {
			throw error
		}
	}
}

