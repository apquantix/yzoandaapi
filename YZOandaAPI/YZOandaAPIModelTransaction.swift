//
//  YZOandaAPIModelTransaction.swift
//  YZOandaAPI
//
//  Created by Bonifatio Hartono on Y17M04D21.
//  Copyright © 2017 Bonifatio Hartono. All rights reserved.
//

import Foundation
public class YZOandaAPIModelTransaction : YZOandaAPIModel {
	public var type : YZOandaAPITransactionType!

	public override init(data: Data) throws {
		do {
			try super.init(data: data)

            let typeStr = try self.dict.letsUnwrapStringValueForKey("type")
            
			guard let aType = YZOandaAPITransactionType(rawValue: typeStr as String) else {
				throw TransactionErr.TypeNotRecognized(type: typeStr as String)
			}

			self.type = aType
		}
		catch {
			throw error
		}
	}
}

