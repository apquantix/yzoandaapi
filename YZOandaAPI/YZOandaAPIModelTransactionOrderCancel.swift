//
//  YZOandaAPIModelTransactionOrderCancel.swift
//  YZOandaAPI
//
//  Created by Selvia Ettine on Y17M04D21.
//  Copyright © 2017 Bonifatio Hartono. All rights reserved.
//

import Foundation

public class YZOandaAPIModelTransactionOrderCancel : YZOandaAPIModelTransaction {
    
    public var reason: YZOandaAPITransactionOrderCancelReason!
    public var orderID : NSString!
    
    
    public init(transaction: YZOandaAPIModelTransaction) throws {
        do {
            try super.init(data: transaction.data)
            
            let reasonStr = try self.dict.letsUnwrapStringValueForKey("reason")
            
            guard let aReason = YZOandaAPITransactionOrderCancelReason(rawValue: reasonStr as String) else {
                throw TransactionErr.ReasonNotRecognized(reason: reasonStr as String)
            }
            
            self.reason = aReason
            
            self.orderID = try self.dict.letsUnwrapStringValueForKey("orderID")

        }
        catch {
            throw error
        }
        
    }
}
