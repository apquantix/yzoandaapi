//
//  YZOandaAPIModelTransactionOrderFill.swift
//  YZOandaAPI
//
//  Created by Selvia Ettine on Y17M04D21.
//  Copyright © 2017 Bonifatio Hartono. All rights reserved.
//

import Foundation

public class YZOandaAPIModelTransactionOrderFill : YZOandaAPIModelTransaction {

    public var reason: YZOandaAPITransactionOrderFillReason!
	public var id : NSString!
    public var orderID : NSString!
	public var instrument : NSString!
    public var price: NSNumber!
    public var units : NSNumber!
    

    public init(transaction: YZOandaAPIModelTransaction) throws {
        do {
            try super.init(data: transaction.data)
            
            let reasonStr = try self.dict.letsUnwrapStringValueForKey("reason")
            
            guard let aReason = YZOandaAPITransactionOrderFillReason(rawValue: reasonStr as String) else {
                throw TransactionErr.ReasonNotRecognized(reason: reasonStr as String)
            }
            
            self.reason = aReason
            
            self.id = try self.dict.letsUnwrapStringValueForKey("id")
            self.orderID = try self.dict.letsUnwrapStringValueForKey("orderID")
            self.instrument = try self.dict.letsUnwrapStringValueForKey("instrument")
            self.price = try self.dict.letsUnwrapStringFloatValueForKey("price")
            self.units = try self.dict.letsUnwrapStringIntegerValueForKey("units")
            
        }
        catch {
            throw error
        }
        
    }
    
}
