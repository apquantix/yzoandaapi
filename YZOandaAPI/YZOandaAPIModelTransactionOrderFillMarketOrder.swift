//
//  YZOandaAPIModelTransactionOrderFillMarketOrder.swift
//  YZOandaAPI
//
//  Created by Selvia Ettine on Y17M04D21.
//  Copyright © 2017 Bonifatio Hartono. All rights reserved.
//

import Foundation

public class YZOandaAPIModelTransactionOrderFillMarketOrder : YZOandaAPIModelTransactionOrderFill {
	
    public init(orderFill: YZOandaAPIModelTransactionOrderFill) throws {
        do {
            try super.init(transaction: orderFill)

        }
        catch {
            throw error
        }
    }
}

