//
//  YZOandaAPIModelTransactionOrderFillMarketOrderTradeClose.swift
//  YZOandaAPI
//
//  Created by Selvia Ettine on Y17M04D22.
//  Copyright © 2017 Bonifatio Hartono. All rights reserved.
//

import Foundation

public class YZOandaAPIModelTransactionOrderFillMarketOrderTradeClose : YZOandaAPIModelTransactionOrderFill {
    
    public var tradesClosed : [YZOandaAPIModelTransactionDefinitionTradeReduce]!
    
    public init(orderFill: YZOandaAPIModelTransactionOrderFill) throws {
        do {
            try super.init(transaction: orderFill)
            
            let aTradesClosed = try self.dict.letsUnwrapArrayOfDictionaryForKey("tradesClosed")
            
            self.tradesClosed = []
            
            for tradeReduceDict in aTradesClosed {
                do {
                    self.tradesClosed.append(try YZOandaAPIModelTransactionDefinitionTradeReduce(dict: tradeReduceDict))
                    
                } catch {
                    throw error
                }
            }
            
        }
        catch {
            throw error
        }
    }
}

public class YZOandaAPIModelTransactionDefinitionTradeReduce : NSObject {
    
    public var tradeID : NSString!
    public var units : NSNumber!
    public var realizedPL : NSNumber!
    public var financing : NSNumber!
    
    
    public init(dict: NSDictionary) throws {
        do {
            self.tradeID = try dict.letsUnwrapStringValueForKey("tradeID")
            self.units = try dict.letsUnwrapStringIntegerValueForKey("units")
            self.realizedPL = try dict.letsUnwrapStringFloatValueForKey("realizedPL")
            self.financing = try dict.letsUnwrapStringFloatValueForKey("financing")
        }
        catch {
            throw error
        }
    }
    
}
