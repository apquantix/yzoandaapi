//
//  YZOandaAPIModelTransactionStopLossOrder.swift
//  YZOandaAPI
//
//  Created by Bonifatio Hartono on Y17M04D21.
//  Copyright © 2017 Bonifatio Hartono. All rights reserved.
//

import Foundation

public class YZOandaAPIModelTransactionStopLossOrder : YZOandaAPIModelTransaction {

	public var reason: YZOandaAPITransactionStopLossOrderReason!
	public var id : NSString!
	public var tradeID : NSString!
	public var price : NSNumber!


	public init(transaction: YZOandaAPIModelTransaction) throws {
		do {
			try super.init(data: transaction.data)

			let reasonStr = try self.dict.letsUnwrapStringValueForKey("reason")
            
			guard let aReason = YZOandaAPITransactionStopLossOrderReason(rawValue: reasonStr as String) else {
				throw TransactionErr.ReasonNotRecognized(reason: reasonStr as String)
			}

			self.reason = aReason
            
            self.id = try self.dict.letsUnwrapStringValueForKey("id")
            self.tradeID = try self.dict.letsUnwrapStringValueForKey("tradeID")
            self.price = try self.dict.letsUnwrapStringFloatValueForKey("price")
            
		}
		catch {
			throw error
		}
	}
}
