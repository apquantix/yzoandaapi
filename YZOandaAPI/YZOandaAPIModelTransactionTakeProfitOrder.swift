//
//  YZOandaAPIModelTransactionTakeProfitOrder.swift
//  YZOandaAPI
//
//  Created by Selvia Ettine on 4/21/17.
//  Copyright © 2017 Bonifatio Hartono. All rights reserved.
//

import Foundation

public class YZOandaAPIModelTransactionTakeProfitOrder : YZOandaAPIModelTransaction {
    
    public var reason: YZOandaAPITransactionTakeProfitOrderReason!
    public var id : NSString!
    public var tradeID : NSString!
    public var price : NSNumber!
    
    
    public init(transaction: YZOandaAPIModelTransaction) throws {
        do {
            try super.init(data: transaction.data)
            
            let reasonStr = try self.dict.letsUnwrapStringValueForKey("reason")
            
            guard let aReason = YZOandaAPITransactionTakeProfitOrderReason(rawValue: reasonStr as String) else {
                throw TransactionErr.ReasonNotRecognized(reason: reasonStr as String)
            }
            
            self.reason = aReason
            
            self.id = try self.dict.letsUnwrapStringValueForKey("id")
            self.tradeID = try self.dict.letsUnwrapStringValueForKey("tradeID")
            self.price = try self.dict.letsUnwrapStringFloatValueForKey("price")
            
        }
        catch {
            throw error
        }
    }
}
