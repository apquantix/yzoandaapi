//
//  YZOandaAPITransactionMarketOrderReason.swift
//  YZOandaAPI
//
//  Created by Selvia Ettine on 4/20/17.
//  Copyright © 2017 Bonifatio Hartono. All rights reserved.
//

import Foundation

/// The reason that the Market Order was created.
public enum YZOandaAPITransactionMarketOrderReason : String {
    
    /// The Market Order was created at the request of a client
    case CLIENT_ORDER = "CLIENT_ORDER"
    /// The Market Order was created to close a Trade at the request of a client
    case TRADE_CLOSE = "TRADE_CLOSE"
    /// The Market Order was created to close a Position at the request of a client
    case POSITION_CLOSEOUT = "POSITION_CLOSEOUT"
    /// The Market Order was created as part of a Margin Closeout
    case MARGIN_CLOSEOUT = "MARGIN_CLOSEOUT"
    /// The Market Order was created to close a trade marked for delayed closure
    case DELAYED_TRADE_CLOSE = "DELAYED_TRADE_CLOSE"

}
