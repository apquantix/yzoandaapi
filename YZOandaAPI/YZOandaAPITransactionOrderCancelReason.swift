//
//  YZOandaAPITransactionOrderCancelReason.swift
//  YZOandaAPI
//
//  Created by Selvia Ettine on 4/19/17.
//  Copyright © 2017 Bonifatio Hartono. All rights reserved.
//

import Foundation

/// The reason that an Order was cancelled.
public enum YZOandaAPITransactionOrderCancelReason : String {
    
    /// The Order was cancelled because at the time of filling, an unexpected internal server error occurred.
    case INTERNAL_SERVER_ERROR = "INTERNAL_SERVER_ERROR"
    /// The Order was cancelled because at the time of filling the account was locked.
    case ACCOUNT_LOCKED = "ACCOUNT_LOCKED"
    /// The order was to be filled, however the account is configured to not allow new positions to be created.
    case ACCOUNT_NEW_POSITIONS_LOCKED = "ACCOUNT_NEW_POSITIONS_LOCKED"
    /// Filling the Order wasn’t possible because it required the creation of a dependent Order and the Account is locked for Order creation.
    case ACCOUNT_ORDER_CREATION_LOCKED = "ACCOUNT_ORDER_CREATION_LOCKED"
    /// Filling the Order was not possible because the Account is locked for filling Orders.
    case ACCOUNT_ORDER_FILL_LOCKED = "ACCOUNT_ORDER_FILL_LOCKED"
    /// The Order was cancelled explicitly at the request of the client.
    case CLIENT_REQUEST = "CLIENT_REQUEST"
    /// The Order cancelled because it is being migrated to another account.
    case MIGRATION = "MIGRATION"
    /// Filling the Order wasn’t possible because the Order’s instrument was halted.
    case MARKET_HALTED = "MARKET_HALTED"
    /// The Order is linked to an open Trade that was closed.
    case LINKED_TRADE_CLOSED = "LINKED_TRADE_CLOSED"
    /// The time in force specified for this order has passed.
    case TIME_IN_FORCE_EXPIRED = "TIME_IN_FORCE_EXPIRED"
    /// Filling the Order wasn’t possible because the Account had insufficient margin.
    case INSUFFICIENT_MARGIN = "INSUFFICIENT_MARGIN"
    /// Filling the Order would have resulted in a a FIFO violation.
    case FIFO_VIOLATION = "FIFO_VIOLATION"
    /// Filling the Order would have violated the Order’s price bound.
    case BOUNDS_VIOLATION = "BOUNDS_VIOLATION"
    /// The Order was cancelled for replacement at the request of the client.
    case CLIENT_REQUEST_REPLACED = "CLIENT_REQUEST_REPLACED"
    /// Filling the Order wasn’t possible because enough liquidity available.
    case INSUFFICIENT_LIQUIDITY = "INSUFFICIENT_LIQUIDITY"
    /// Filling the Order would have resulted in the creation of a Take Profit Order with a GTD time in the past.
    case TAKE_PROFIT_ON_FILL_GTD_TIMESTAMP_IN_PAST = "TAKE_PROFIT_ON_FILL_GTD_TIMESTAMP_IN_PAST"
    /// Filling the Order would result in the creation of a Take Profit Order that would have been filled immediately, closing the new Trade at a loss.
    case TAKE_PROFIT_ON_FILL_LOSS = "TAKE_PROFIT_ON_FILL_LOSS"
    /// Filling the Order would result in the creation of a Take Profit Loss Order that would close the new Trade at a loss when filled.
    case LOSING_TAKE_PROFIT = "LOSING_TAKE_PROFIT"
    /// Filling the Order would have resulted in the creation of a Stop Loss Order with a GTD time in the past.
    case STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST = "STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST"
    /// Filling the Order would result in the creation of a Stop Loss Order that would have been filled immediately, closing the new Trade at a loss.
    case STOP_LOSS_ON_FILL_LOSS = "STOP_LOSS_ON_FILL_LOSS"
    /// Filling the Order would have resulted in the creation of a Trailing Stop Loss Order with a GTD time in the past.
    case TRAILING_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST = "TRAILING_STOP_LOSS_ON_FILL_GTD_TIMESTAMP_IN_PAST"
    /// Filling the Order would result in the creation of a new Open Trade with a client Trade ID already in use.
    case CLIENT_TRADE_ID_ALREADY_EXISTS = "CLIENT_TRADE_ID_ALREADY_EXISTS"
    /// Closing out a position wasn’t fully possible.
    case POSITION_CLOSEOUT_FAILED = "POSITION_CLOSEOUT_FAILED"
    /// Filling the Order would cause the maximum open trades allowed for the Account to be exceeded.
    case OPEN_TRADES_ALLOWED_EXCEEDED = "OPEN_TRADES_ALLOWED_EXCEEDED"
    /// Filling the Order would have resulted in exceeding the number of pending Orders allowed for the Account.
    case PENDING_ORDERS_ALLOWED_EXCEEDED = "PENDING_ORDERS_ALLOWED_EXCEEDED"
    /// Filling the Order would have resulted in the creation of a Take Profit Order with a client Order ID that is already in use.
    case TAKE_PROFIT_ON_FILL_CLIENT_ORDER_ID_ALREADY_EXISTS = "TAKE_PROFIT_ON_FILL_CLIENT_ORDER_ID_ALREADY_EXISTS"
    /// Filling the Order would have resulted in the creation of a Stop Loss Order with a client Order ID that is already in use.
    case STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_ALREADY_EXISTS = "STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_ALREADY_EXISTS"
    /// Filling the Order would have resulted in the creation of a Trailing Stop Loss Order with a client Order ID that is already in use.
    case TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_ALREADY_EXISTS = "TRAILING_STOP_LOSS_ON_FILL_CLIENT_ORDER_ID_ALREADY_EXISTS"
    /// Filling the Order would have resulted in the Account’s maximum position size limit being exceeded for the Order’s instrument.
    case POSITION_SIZE_EXCEEDED = "POSITION_SIZE_EXCEEDED"
    
}
