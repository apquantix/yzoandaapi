//
//  YZOandaAPITransactionOrderFillReason.swift
//  YZOandaAPI
//
//  Created by Selvia Ettine on 4/20/17.
//  Copyright © 2017 Bonifatio Hartono. All rights reserved.
//

import Foundation

///The reason that an Order was filled
public enum YZOandaAPITransactionOrderFillReason : String {

    /// The Order filled was a Limit Order
    case LIMIT_ORDER = "LIMIT_ORDER"
    /// The Order filled was a Stop Order
    case STOP_ORDER = "STOP_ORDER"
    /// The Order filled was a Market-if-touched Order
    case MARKET_IF_TOUCHED_ORDER = "MARKET_IF_TOUCHED_ORDER"
    /// The Order filled was a Take Profit Order
    case TAKE_PROFIT_ORDER = "TAKE_PROFIT_ORDER"
    /// The Order filled was a Stop Loss Order
    case STOP_LOSS_ORDER = "STOP_LOSS_ORDER"
    /// The Order filled was a Trailing Stop Loss Order
    case TRAILING_STOP_LOSS_ORDER = "TRAILING_STOP_LOSS_ORDER"
    /// The Order filled was a Market Order
    case MARKET_ORDER = "MARKET_ORDER"
    /// The Order filled was a Market Order used to explicitly close a Trade
    case MARKET_ORDER_TRADE_CLOSE = "MARKET_ORDER_TRADE_CLOSE"
    /// The Order filled was a Market Order used to explicitly close a Position
    case MARKET_ORDER_POSITION_CLOSEOUT = "MARKET_ORDER_POSITION_CLOSEOUT"
    /// The Order filled was a Market Order used for a Margin Closeout
    case MARKET_ORDER_MARGIN_CLOSEOUT = "MARKET_ORDER_MARGIN_CLOSEOUT"
    /// The Order filled was a Market Order used for a delayed Trade close
    case MARKET_ORDER_DELAYED_TRADE_CLOSE = "MARKET_ORDER_DELAYED_TRADE_CLOSE"

}
