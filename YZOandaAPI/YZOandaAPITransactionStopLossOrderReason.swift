//
//  YZOandaAPITransactionStopLossOrderReason.swift
//  YZOandaAPI
//
//  Created by Selvia Ettine on 4/20/17.
//  Copyright © 2017 Bonifatio Hartono. All rights reserved.
//

import Foundation

/// The reason that the Stop Loss Order was initiated
public enum YZOandaAPITransactionStopLossOrderReason : String {

    /// The Stop Loss Order was initiated at the request of a client
    case CLIENT_ORDER = "CLIENT_ORDER"
    /// The Stop Loss Order was initiated as a replacement for an existing Order
    case REPLACEMENT = "REPLACEMENT"
    /// The Stop Loss Order was initiated automatically when an Order was filled that opened a new Trade requiring a Stop Loss Order.
    case ON_FILL = "ON_FILL"

}
