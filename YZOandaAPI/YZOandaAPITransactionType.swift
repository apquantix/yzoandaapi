//
//  YZOandaAPITransactionType.swift
//  YZOandaAPI
//
//  Created by Selvia Ettine on 4/19/17.
//  Copyright © 2017 Bonifatio Hartono. All rights reserved.
//

import Foundation

/// The possible types of a transaction
public enum YZOandaAPITransactionType : String {
    
    /// Account Create Transaction
    case CREATE = "CREATE"
    /// Account Close Transaction
    case CLOSE = "CLOSE"
    /// Account Reopen Transaction
    case REOPEN = "REOPEN"
    /// Client Configuration Transaction
    case CLIENT_CONFIGURE = "CLIENT_CONFIGURE"
    /// Client Configuration Reject Transaction
    case CLIENT_CONFIGURE_REJECT = "CLIENT_CONFIGURE_REJECT"
    /// Transfer Funds Transaction
    case TRANSFER_FUNDS = "TRANSFER_FUNDS"
    /// Transfer Funds Reject Transaction
    case TRANSFER_FUNDS_REJECT = "TRANSFER_FUNDS_REJECT"
    /// Market Order Transaction
    case MARKET_ORDER = "MARKET_ORDER"
    /// Market Order Reject Transaction
    case MARKET_ORDER_REJECT = "MARKET_ORDER_REJECT"
    /// Limit Order Transaction
    case LIMIT_ORDER = "LIMIT_ORDER"
    /// Limit Order Reject Transaction
    case LIMIT_ORDER_REJECT = "LIMIT_ORDER_REJECT"
    /// Stop Order Transaction
    case STOP_ORDER = "STOP_ORDER"
    /// Stop Order Reject Transaction
    case STOP_ORDER_REJECT = "STOP_ORDER_REJECT"
    /// Market if Touched Order Transaction
    case MARKET_IF_TOUCHED_ORDER = "MARKET_IF_TOUCHED_ORDER"
    /// Market if Touched Order Reject Transaction
    case MARKET_IF_TOUCHED_ORDER_REJECT = "MARKET_IF_TOUCHED_ORDER_REJECT"
    /// Take Profit Order Transaction
    case TAKE_PROFIT_ORDER = "TAKE_PROFIT_ORDER"
    /// Take Profit Order Reject Transaction
    case TAKE_PROFIT_ORDER_REJECT = "TAKE_PROFIT_ORDER_REJECT"
    /// Stop Loss Order Transaction
    case STOP_LOSS_ORDER = "STOP_LOSS_ORDER"
    /// Stop Loss Order Reject Transaction
    case STOP_LOSS_ORDER_REJECT = "STOP_LOSS_ORDER_REJECT"
    /// Trailing Stop Loss Order Transaction
    case TRAILING_STOP_LOSS_ORDER = "TRAILING_STOP_LOSS_ORDER"
    /// Trailing Stop Loss Order Reject Transaction
    case TRAILING_STOP_LOSS_ORDER_REJECT = "TRAILING_STOP_LOSS_ORDER_REJECT"
    /// Order Fill Transaction
    case ORDER_FILL = "ORDER_FILL"
    /// Order Cancel Transaction
    case ORDER_CANCEL = "ORDER_CANCEL"
    /// Order Cancel Reject Transaction
    case ORDER_CANCEL_REJECT = "ORDER_CANCEL_REJECT"
    /// Order Client Extensions Modify Transaction
    case ORDER_CLIENT_EXTENSIONS_MODIFY = "ORDER_CLIENT_EXTENSIONS_MODIFY"
    /// Order Client Extensions Modify Reject Transaction
    case ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT = "ORDER_CLIENT_EXTENSIONS_MODIFY_REJECT"
    /// Trade Client Extensions Modify Transaction
    case TRADE_CLIENT_EXTENSIONS_MODIFY = "TRADE_CLIENT_EXTENSIONS_MODIFY"
    /// Trade Client Extensions Modify Reject Transaction
    case TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT = "TRADE_CLIENT_EXTENSIONS_MODIFY_REJECT"
    /// Margin Call Enter Transaction
    case MARGIN_CALL_ENTER = "MARGIN_CALL_ENTER"
    /// Margin Call Extend Transaction
    case MARGIN_CALL_EXTEND = "MARGIN_CALL_EXTEND"
    /// Margin Call Exit Transaction
    case MARGIN_CALL_EXIT = "MARGIN_CALL_EXIT"
    /// Delayed Trade Closure Transaction
    case DELAYED_TRADE_CLOSURE = "DELAYED_TRADE_CLOSURE"
    /// Daily Financing Transaction
    case DAILY_FINANCING = "DAILY_FINANCING"
    /// Reset Resettable PL Transaction
    case RESET_RESETTABLE_PL = "RESET_RESETTABLE_PL"

	/// A TransactionHeartbeat object is injected into the Transaction stream to ensure that the HTTP connection remains active.
	case HEARTBEAT = "HEARTBEAT"
}
