//
//  YZOandaAPIUserToken.swift
//  YZOandaAPI
//
//  Created by Bonifatio Hartono on Y17M03D21.
//  Copyright © 2017 Bonifatio Hartono. All rights reserved.
//

import Foundation



public final class YZOandaAPIUserToken {


	/// shared user token
	public static let shared = YZOandaAPIUserToken()

	/// backup token instance
	private var userToken : String?

	private var userTokenSetter : (String) -> () = { _ in }
	private var userTokenGetter : () -> (String?) = { _ in return nil}

	public func getUserToken() -> String?{

		if let defaultsToken = userTokenGetter() {
			userToken = defaultsToken
			return defaultsToken
		}
		else {
			if let instanceToken = self.userToken {
				return instanceToken
			}
			else{
				return nil
			}
		}
	}

	public func setUserToken(with: String) -> () {
		userToken = with
		self.userTokenSetter(with)
	}

	public func initializeGetter(getter: @escaping () -> String?) {
		userTokenGetter = getter
	}

	public func initializeSetter(setter: @escaping (String) -> ()){
		userTokenSetter = setter
	}
}
