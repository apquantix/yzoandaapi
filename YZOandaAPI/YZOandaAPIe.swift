//
//  YZOandaAPIe.swift
//  YZOandaAPI
//
//  Created by Bonifatio Hartono on Y17M04D27.
//  Copyright © 2017 Bonifatio Hartono. All rights reserved.
//

import Foundation

public enum YZOandaAPIeScheme : String {
	case HTTPS = "https"
}

public enum YZOandaAPIeMachine : String {
	case fxTradeMachineName = "api-fxtrade."
	case fxTradeStreamMachineName = "stream-fxtrade."
	case fxTradePracticeMachineName = "api-fxpractice."
	case fxTradePracticeStreamMachineName = "stream-fxpractice."
}

public enum YZOandaAPIeDomain : String {
	case Oanda = "oanda.com"
}

public enum YZOandaAPIeVersion : String {
	case v3 = "/v3"
}

public enum YZOandaAPIeAccount : String {
    case summary = "/summary"
    case instruments = "/instruments"
    case configuration = "/configuration"
    case changes = "/changes"
    case orders = "/orders"
    case pendingOrders = "/pendingOrders"
    case trades = "/trades"
    case openTrades = "/openTrades"
    case positions = "/positions"
    case openPositions = "/openPositions"
    case pricing = "/pricing"
    case transactions = "/transactions"
}
