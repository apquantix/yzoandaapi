//
//  YZOandaAPIeOrder.swift
//  YZOandaAPI
//
//  Created by Bonifatio Hartono on Y17M04D28.
//  Copyright © 2017 Bonifatio Hartono. All rights reserved.
//

import Foundation

/* 
	Order-related Definitions
	Reference: http://developer.oanda.com/rest-live-v20/order-df/
*/

/// The type of the Order.
public enum YZOandaAPIeOrderType : String {
	// A Market Order 
	case MARKET = "MARKET"
	// A Limit Order 
	case LIMIT = "LIMIT"
	// A Stop Order 
	case STOP = "STOP"
	// A Market-if-touched Order 
	case MARKET_IF_TOUCHED = "MARKET_IF_TOUCHED"
	// A Take Profit Order 
	case TAKE_PROFIT = "TAKE_PROFIT"
	// A Stop Loss Order 
	case STOP_LOSS = "STOP_LOSS"
	// A Trailing Stop Loss Order 
	case TRAILING_STOP_LOSS = "TRAILING_STOP_LOSS"
}

/// The current state of the Order.
public enum YZOandaAPIeOrderState : String {

	// The Order is currently pending execution 
	case PENDING = "PENDING"
	// The Order has been filled 
	case FILLED = "FILLED"
	// The Order has been triggered 
	case TRIGGERED = "TRIGGERED"
	// The Order has been cancelled 
	case CANCELLED = "CANCELLED"
}

/// The time-in-force of an Order. 
/// TimeInForce describes how long an Order should remain pending before being automatically cancelled by the execution system.
public enum YZOandaAPIeOrdersTimeInForce : String {

	// The Order is "Good unTil Cancelled"
	case GTC = "GTC"
	// The Order is "Good unTil Date" and will be cancelled at the provided time
	case GTD = "GTD"
	// The Order is "Good For Day" and will be cancelled at 5pm New York time
	case GFD = "GFD"
	// The Order must be immediately "Filled Or Killed"
	case FOK = "FOK"
	// The Order must be "Immediatedly paritally filled Or Cancelled"
	case IOC = "IOC"
}


/// Specification of how Positions in the Account are modified when the Order is filled.
public enum YZOandaAPIeOrderPositionFill : String {

	// When the Order is filled, only allow Positions to be opened or extended. 
	case OPEN_ONLY = "OPEN_ONLY"
	// When the Order is filled, always fully reduce an existing Position before opening a new Position. 
	case REDUCE_FIRST = "REDUCE_FIRST"
	// When the Order is filled, only reduce an existing Position. 
	case REDUCE_ONLY = "REDUCE_ONLY"
	// When the Order is filled, use REDUCE_FIRST behaviour for non-client hedging Accounts, and OPEN_ONLY behaviour for client hedging Accounts. 
	case DEFAULT = "DEFAULT"
}

/// Specification of what component of a price should be used for comparison when determining if the Order should be filled.
public enum YZOandaAPIeOrderTriggerCondition : String {

	// Trigger an Order the “natural” way: compare its price to the ask for long Orders and bid for short Orders. 
	case DEFAULT = "DEFAULT"
	// Trigger an Order the opposite of the “natural” way: compare its price the bid for long Orders and ask for short Orders. 
	case INVERSE = "INVERSE"
	// Trigger an Order by comparing its price to the bid regardless of whether it is long or short. 
	case BID = "BID"
	// Trigger an Order by comparing its price to the ask regardless of whether it is long or short. 
	case ASK = "ASK"
	// Trigger an Order by comparing its price to the midpoint regardless of whether it is long or short. 
	case MID = "MID"
}


